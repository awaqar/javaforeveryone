package Chapter_6;

public class P6_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] random = new int[10];
		
		//fill the array with random integers
		for(int i=0; i<random.length;i++){
			random[i] = (int)(Math.random()*20)+1;
			System.out.println(random[i]);
		}
		int i =0;
		System.out.println("Generating numbers at even indexes");
		do{
			System.out.println("Index" + i + ": " + random[i]);
			i=i+2;
		}while(i<random.length);
		
		//Printing all elements that are even in the array random[].
		System.out.println("Printing all even elements: ");
		
		for(int j= 0; j<random.length;j++){
			if(random[j] %2 ==0) 
				System.out.println(random[j]);
		}
		System.out.println("REversed: ");
		//printing all the elements of array random[] in reverse order
		for(int x = random.length-1; x>=0; x--){
			System.out.println(random[x]);
		}
		
		System.out.println("Printing first and last element: ");
		//printing only the first and the last element
		for(int x=0; x<random.length;x++){
			if(x == 0 || x == random.length-1){
				System.out.println(random[x]);
			}
		}
	}

	
}
