package Chapter_6;

import java.util.Scanner;

public class HowTo6_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//read inputs
		//get the minimum value
		//sum the total
		int [] array = readInput(7);
		int min = minimum(array);
		int total = total(array);
		
		System.out.println("Final score: " + (total - min));
		
		
	}
	
	/**
	 * reads n number of inputs and returns them as an array
	 * @param number of inputs
	 * @return array of inputs
	 */
	public static int[] readInput(int n){
		System.out.println("Enter "+ n + " numbers");
		Scanner in = new Scanner(System.in);
		int[] input = new int[n];
		for(int i=0;i<input.length;i++){
			input[i]= in.nextInt();
		}
		return input;
	}
	
	
	/**
	 * marks the minimum from a given array
	 */
	public static int minimum(int[] array){
		
		int min = array[0];
		
		for(int i=1;i<array.length;i++){
			if(array[i]<min){
				min = array[i];
				
			}
		}
		
		return min;
		
	}
	
	/**returns the total of the given array
	 * @param an int array
	 * @return total
	 */
	public static int total(int[]array){
		int total=0;
		for(int e:array){
			total+=e;
		}
		return total;
	}
	
	
	
	

}
