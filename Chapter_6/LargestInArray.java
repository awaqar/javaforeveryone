package Chapter_6;

import java.util.Scanner;

public class LargestInArray {

	public static void main(String[] args) {
		final int LENGTH = 10;
		double [] data = new double[LENGTH];
		int currentsize = 0;
		
		//read inputs
		System.out.println("Please enter values, Q to quit: ");
		Scanner in = new Scanner(System.in);
		while(in.hasNextDouble() && currentsize < data.length){
			data[currentsize] = in.nextDouble();
			currentsize++;
		}
		
		//Find the largest
		double largest = data[0];
		for(int i=1; i<data.length;i++){
			if(data[i]>largest){
				largest = data[i];
			}
		}
		
		//print all values marking the largest
		for(int i=0; i <currentsize;i++){
			System.out.print(data[i]);
			if(data[i]==largest){
				System.out.print("<== Largest");
			}
			System.out.println();
			
		}

	}

}
