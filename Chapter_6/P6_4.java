package Chapter_6;
import java.util.Scanner;
/**
 * This program prints the alternating sum of all elements in an array.
 * For example: if your program reads the input
 * 	1 4 9 16 9 7 4 9 11
 * then it computes
 * 1 - 4 + 9 - 16 + 9 - 7 + 4 - 9 + 11*/


public class P6_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] data = new int[10];
		int sum=0;
		Scanner in = new Scanner(System.in);
		System.out.println("Enter values");
		//read input values
		int size = 0;
		while(size<data.length && in.hasNextInt()){
			data[size] = in.nextInt();
			size++;
		}
		
		//compute the alternating sum
		for(int i=0;i<data.length;i++){
			if(i%2==0){
				sum = sum+ data[i];
			}
			else{
				sum= sum- data[i];
			}
		}
		
		System.out.println(sum);
		
	}

}
