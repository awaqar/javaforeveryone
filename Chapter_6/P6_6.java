package Chapter_6;

import java.util.Arrays;

/**This program creates a method remove which counts the
 * number of duplicate values and removes them from the array
 * to get number of remaining elements we would simply
 * minus the current size of the array from the number of duplicate elements*/
public class P6_6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] data = {1,4,9,16,9,7,4,9,11,0,0,0};
		System.out.println(remove(data));
		//check to see if the duplicates have been removed! 
		System.out.println(Arrays.toString(data));
	}
	
	/**
	 * removes the duplicate values in an array
	 * @param array an integer array
	 * @return integer number of duplicate elements*/
	public static int remove(int[] array){
		int elements=0;
		int size = array.length;
		for(int i =0; i<size;i++){
			for(int j = i+1; j<size;j++){
				if(array[i]==array[j]){
					//overwrite it and replace it with the last element
					array[j] = array[size-1];
					array[size-1]=0;
					size--;
					elements++;
				}
			}
		}
		return elements;
	}

}
