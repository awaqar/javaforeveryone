package Chapter_6;

import java.util.ArrayList;

public class P6_14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> arraylist = new ArrayList<Integer>();
		arraylist.add(15);
		arraylist.add(40);
		arraylist.add(25);
		arraylist.add(20);
		arraylist.add(10);
		
		printAsterisk(arraylist);
		
	}
	
	/**
	 * prints a bar chart of the values in an array list using asterisk '*'
	 * @param ArrayList<Integer> array
	 */
	public static void printAsterisk(ArrayList<Integer> array){
		
		for(int j=0; j<array.size();j++){
			for(int i=0; i< array.get(j);i++){
				System.out.print("*");
			}
			System.out.println();
		}
	}

}
