package Chapter_6;

import java.util.Scanner;

public class ReadInput {
	
	public static void main(String[] args) {
		System.out.println(readInput(10));
	}
	
	public static int readInput(int n){
		Scanner in = new Scanner(System.in);
		int sum=0;
		int[] array = new int[n];
		int i=0;
		while(i<n && in.hasNextDouble()){
			array[i] = in.nextInt(); sum = sum+array[i];
			i++;
		}
		
		return sum;
		
	}

}
