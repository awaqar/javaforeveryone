package Chapter_6;

import java.util.Arrays;

public class copyArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double [] array = new double[10];
		
		for(int i=0; i<array.length;i++){
			array[i] = Math.ceil((Math.random()*20) + 1);
		}
		
		System.out.println(Arrays.toString(array));
		double[] numbers = Arrays.copyOf(array, array.length);
		
		for(double e : numbers){
			System.out.println(e);
		}
		
	}

}
