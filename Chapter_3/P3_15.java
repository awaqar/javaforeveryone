package Chapter_3;

import java.util.Scanner;

public class P3_15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		
		System.out.print("Name: ");
		String name = in.next();
		
		System.out.print("Salary");
		double salary_per_hour = in.nextDouble();
		
		System.out.print("how many hours did you work last week? ");
		double hours_worked = in.nextDouble();
		
		final double BONUS_PERCENT = 1.5 * salary_per_hour;
		double pay_of_40 = 40 * salary_per_hour;
		
		double overtime = 0;
		double overtime_pay = 0;
		double totalpay;
		double monthly;
		double yearly;
		
		if(hours_worked > 40){
			overtime = hours_worked - 40;
			overtime_pay = BONUS_PERCENT * overtime;
			totalpay = pay_of_40 + overtime_pay;
		}
		
		else
			{totalpay = hours_worked * salary_per_hour;}
		
		monthly = 4 * totalpay;
		yearly = monthly * 12;
				
		System.out.println("Pay Check for " + name);
		System.out.println("Total Hours worked last week: "+ hours_worked);
		System.out.println("OverTime hours worked: " + overtime);
		System.out.println("Pay for Overtime:" + overtime_pay);
		System.out.println("Total Pay: "+ totalpay);
		System.out.println("Monthly: " + monthly);
		System.out.println("Yearly: " + yearly);
		
		
	}

}
