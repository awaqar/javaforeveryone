package Chapter_3;

import java.text.DecimalFormat;
import java.util.Scanner;

public class P3_13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			Scanner in = new Scanner(System.in);
			System.out.print("Enter digit1: ");
			float n1 = in.nextFloat();
			
			System.out.print("Enter number 2: ");
			float n2 = in.nextFloat();
			
			String same ="SAME";
			String diff = "Different";
			String result ="They are ";
			
			DecimalFormat df = new DecimalFormat("#.00");
			if(df.format(n1) == df.format(n2)){
				result = result + same;
			}
			
			else
				result = result + diff;
			
			System.out.println(result);
	}

}
