package Chapter_3;

import java.util.Scanner;

public class P3_23 {
	
	public static void main(String []args){
		Scanner in = new Scanner(System.in);
		System.out.print("month: ");
		int month;
		int days = 0;
		// enter a month 
		// while it is not a number keep asking the user for input
		//1,3,5,7,8,10,12 = 31 days
		//4,6,9,11 = 30 days
		// 2 28/29 days
		
		if(in.hasNextInt()){
				month = in.nextInt();
				
			if(month >0 && month<=12){
				if(month == 2){
					days = 28;
				}
				
				else if(month == 4 || month ==6 || month ==9 || month ==11){
					days = 30;
				}
				else{
					days = 31;
				}
				
				System.out.println(days);
			}
			else{
				System.out.println("Enter a month between 1 and 12!");
			}
		}
		
		else{
			System.out.println("Invalid input!");
		}
			
	}
}


