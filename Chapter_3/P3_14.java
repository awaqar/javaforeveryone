package Chapter_3;

import java.util.Scanner;

public class P3_14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		System.out.print("Enter balance C: ");
		
		int balance_checking = in.nextInt();
		
		System.out.print("Enter balance S: ");
		int balance_saving = in.nextInt();
		
		while(balance_checking <0 || balance_saving <0 ){
			System.out.print("Enter balance C: ");
			balance_checking = in.nextInt();
			System.out.print("Enter balance S: ");
			 balance_saving = in.nextInt();
		}
		
		//ask for transaction type Transfer/Withdraw/Debit 
		System.out.print("Which Transation Type: ");
		String trans_type = in.next();
		
		//ask for account type
		System.out.print("Which account type? ");
		String account = in.next();
		
		//ask for the amount to debit/withdraw/transfer
		System.out.print("How much would you like to: ");
		int amount = in.nextInt();
		
		if (account.equals("C")){
			if(trans_type.equals("D")){
				balance_checking = balance_checking + amount;
			}
			
			else if(trans_type.equals("W")){
				
				if(balance_checking - amount < 0){
					System.out.println("cannot complete transaction - negative balance");
				}
				else{
					balance_checking = balance_checking - amount;
				}
			}
			
			else if(trans_type.equals("T")){
				if(balance_checking - amount <0){
					System.out.println("cannot complete transfer - negative balance");
				}
				else{
					balance_checking = balance_checking - amount;
					balance_saving = balance_saving + amount;
				}
			}
			
			else{
				System.out.println("Unidentified Transaction option, enter T|W|D");
			}
			
			System.out.println("Your current balance after transaction: "+ balance_checking);
			System.out.println("Account Saving: " + balance_saving);
		}
		
		else if(account.equals("S")){
			System.out.println("Savings");
		}
		
		else{
			System.exit(0);
		}
		
		
		System.out.println();
		
		
	}

}
