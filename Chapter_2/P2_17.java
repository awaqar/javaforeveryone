package Chapter_2;

import java.util.Scanner;

public class P2_17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		
		System.out.print("Integer: ");
		int n = in.nextInt();//16384
		int first = n%10;  // Remainder 4
		n /= 10;			// 1638
		int second = n%10; //Remainder 8 
		n/=10;			//163
		int third = n%10;  //Remainder = 3
		n /= 10; 			//16
		int fourth = n%10; //6
		
		int fifth = n/10;
		
		System.out.printf("%d %d %d %d %d", fifth,fourth,third,second,first);
	}

}