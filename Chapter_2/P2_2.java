package Chapter_2;

import java.util.Scanner;

/**
 * Write a program that:
 * prompts the user for two integers
 * prints the sum, difference, product, average, distance (absolute) , max and min*/
public class P2_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		System.out.print("Enter first digit: ");
		int n1 = in.nextInt();
		System.out.println();
		System.out.print("Enter second digit: ");
		int n2 = in.nextInt();
		
		int sum = n1 + n2;
		int dif = n1 - n2;
		int product = n1 * n2;
		int avg = Math.round(sum/2);
		int dist= Math.abs(dif);
		int max = Math.max(n1, n2);
		int min = Math.min(n1, n2);
		
		System.out.print( sum);
		System.out.println();
		
		System.out.printf("%-12s%3d","Difference", dif);
		System.out.println();
		
		System.out.printf("%-12s%3d","Product:", product);
		System.out.println();
		
		System.out.printf("%-12s%3d","Average:", avg);
		System.out.println();
		
		System.out.printf("%-12s%3d","Distance:", dist);
		System.out.println();
		
		System.out.printf("%-12s%3d","Max:", max);
		System.out.println();
		
		System.out.printf("%-12s%3d","Min", min);
		System.out.println();
		
	}

}
