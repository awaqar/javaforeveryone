package Chapter_2;

import java.util.Scanner;

public class P2_15 {

	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		
		System.out.print("Enter between 1,000 and 999,999: ");
		String str =  in.next();
		
		int length = str.length();
		
		String sub1  = str.substring(0,length-4);
		String sub2 = str.substring(length-3);
		
		System.out.println(sub1);
		System.out.println(sub2);
		System.out.println(sub1 + sub2);
	}

}
