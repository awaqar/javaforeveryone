package Chapter_7;

import java.util.ArrayList;


/**
 * P7_15
 * */
public class Mailbox {
	private ArrayList<Message> messages;
	
	public Mailbox(){
		messages = new ArrayList<Message>();
	}
	
	public void addMessage(Message msg){
		messages.add(msg);
	}
	
	public Message getMessage(int i){
		
		return messages.get(i);
	}
	
	public void showMessages(){
		System.out.println("MESSAGE-BOX");
		
		for(Message m:messages){
			System.out.println(m.getSender());
		}
	}
	public void deleteMessage(int i){
		messages.remove(i);
	}
	
}
