package Chapter_7;

public class Cash_Register {
	private int count;
	private double total;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Cash_Register reg1 = new Cash_Register();
		Cash_Register reg2 = new Cash_Register();
		reg1.addItem(0.95); reg1.addItem(0.95);
		System.out.println("Count: " + reg1.getCount());
		System.out.println("Total: " + reg1.getTotal());
	}
	
	public Cash_Register(){
		total = 0;
		count = 0;
	}
	public void addItem(double price){
		total += price;
		count++;
	}

	public int getCount(){
		
		return count;
	}
	
	public double getTotal(){
		
		return total;
	}
	public void clear(){
		total =0;
		count =0;
	}
}
