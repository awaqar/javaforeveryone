package Chapter_7;

import java.util.ArrayList;

/**
 * P7.10 - Implement a class Student. For the purpose of this exercise, a student has a name and
 * a total quiz score. Supply an appropriate constructor and mehtods getName(), addQuiz(int score),
 * getTotalScore(), and getAvergaeScore(). To compute the latter, you also need to store the number of
 * quizes that the student took
 */
public class Student {

	private String name;
	private int totalScore;
	
	private ArrayList<Integer> quiz;
	
	public Student(String name){
		quiz = new ArrayList<Integer>();
		totalScore =0;
		this.name = name;
		
	}
	
	public String getName(){
		return name;
	}
	
	public void addQuiz(int score){
		quiz.add(score);
	}
	
	public int getTotalScore(){
		for(int e:quiz){
			totalScore += e;
		}
		return totalScore;
	}
	
	public int getAverageScore(){
		return totalScore/quiz.size();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student s1 = new Student("Usman");
		System.out.println(s1.getName());
		s1.addQuiz(3);
		s1.addQuiz(3);
		s1.addQuiz(2);
		s1.addQuiz(4);
		System.out.println("Total: "+ s1.getTotalScore());
		System.out.println("Avg: "+s1.getAverageScore());
	}

}
