package Chapter_7;

import java.util.ArrayList;

public class P7_1 {
	private double total;
	private int centsTotal;
	private double salesTotal;
	private static ArrayList<Double> array = new ArrayList<Double>();
	
	
	
	private ArrayList<Double> items;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		P7_1 reg = new P7_1();
		reg.addItem(0.95);
		reg.addItem(0.95);
		System.out.println("Items: " + reg.getCount());
		System.out.println("Total: " + reg.getTotal());
		System.out.println("Cents: " + reg.getCentsTotal());
		//reg.displayAll();
		//reg.clear();
		
		P7_1 reg1 = new P7_1();
		reg1.addItem(1.75);
		reg1.addItem(0.25);
		System.out.println(reg1.getTotal());
		System.out.println(reg1.getSalesTotal());
		
		new P7_1();
		
		System.out.println("Total items: "+ P7_1.array.size());
		

	}
	
	public P7_1(){
		items = new ArrayList<Double>();
		
		total = 0;
		centsTotal = 0;
		salesTotal=0;
		
		
	}
	public void addItem(double price){
		items.add(price);
		array.add(price);
	}
	
	public int getCentsTotal(){
		return centsTotal = (int)(total * 100);
	}
	public double getTotal(){
		for(double e:items){
			total += e;
		}
		return total;
	}
	
	public  double getSalesTotal(){
		for(double e:array){
			salesTotal += e;
		}
		return salesTotal;
		
		
	}
	
	public int getCount(){
		return items.size();
	}
	
	public int getTotalCount(){
		return array.size();
	}
	
	public int getSalesCount(){
		return array.size();
	}
	
	public void displayAll(){
		System.out.println(items);
	}
	
	public void clear(){
		items.clear();
		if(items.size()==0){
			System.out.println("Cash Cleared!");
			
		}
		
	}
	

}
