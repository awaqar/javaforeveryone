/**
 * 
 */
package Chapter_7;
	
/**
 * @author azzumwaqar
 * P7_8: Implement a class SodaCan with methods getSurfaceArea() and
 * getVolume(). In the Constructor, supply height and radius of the can.
 */
public class SodaCan {
	private double height;
	private double radius;
	
	public SodaCan(double height,double radius){
		this.height = height;
		this.radius = radius;
	}
	
	public double getSurfaceAreA(){
		double area = (2 * Math.PI * radius * height)+ (2*Math.PI *radius*radius);
		return Math.round(area);
	}
	
	public double getVolume(){
		return Math.round(Math.PI * (radius * radius) * height);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SodaCan soda = new SodaCan(3,3);
		System.out.println(soda.getSurfaceAreA());
		System.out.println(soda.getVolume());
	}

}
