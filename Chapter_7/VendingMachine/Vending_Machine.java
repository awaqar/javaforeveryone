package Chapter_7;

import java.util.ArrayList;
import java.util.Scanner;

public class Vending_Machine {
	
	private ArrayList<Product> products;
	private int product_number;
	Scanner in;
	double paid;
	public Vending_Machine(){
		this.products = new ArrayList<Product>();
		this.product_number=0;
		 in = new Scanner(System.in);
	}
	
	/**
	 * @param product object
	 * @param int quantity
	 * add Product object to ArrayList<Product>
	 * set products Quantity via Product method: setQty(int qty)*/
	
	public void addProducts(Product p,int qty){
		products.add(p);
		p.setQty(qty);
	}
	
	/***/
	public void showProducts(){
		if(products.size()>0){
			int counter = 0;
			System.out.println("Product\t"+"\tPrice\t"+"Qty");
			for(Product p:products){
				
				System.out.print(counter + ") " +p.getName()+ " \t");
				System.out.println("�"+p.getPrice() + " \t"+ p.getQty());
				counter++;
			}
			//enter product number to buy
				product_number=ask();
				System.out.println("Amont due: �"+ products.get(product_number).getPrice());
				
				pay(products.get(product_number).getPrice(),0);
		}
	}
	
	/**
	 **/
	
		public double pay(double price,double paid){
		
			double remaining;
			double change =0;
			System.out.print("Enter payment: �");
			double amount_input = in.nextDouble();
			
			
			if(amount_input<=0) return pay(price,paid);
			
			else if(amount_input<price){
				 
				paid = paid+amount_input;
				 remaining = price - amount_input;
				 System.out.println("Amount Due: �"+remaining);
				 System.out.println("Amount Paid: �"+ paid);
				 return pay(remaining,paid);
			}
				 
			else if(amount_input>=price){
				change = amount_input -price;
				System.out.println("Change:�"+ change+ "\nThank you for shopping!");
			}
			
			return change;
		}
	
	
	/**
	 * method ask takes no parameters
	 * @return product_number from products array*/
		
	private int ask (){
		
		int input;
		 do{
			 System.out.print("Enter product number: ");
			 input = in.nextInt();
			 
		 }while(input<0 || input >products.size());
		return input;
	}
	
	
	
}
