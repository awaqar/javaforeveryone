package Chapter_7;

public class Product {
	private String name;
	private Double price;
	private int qty;
	
	public Product(){
		name = "";
		price = 0.0;
	}
	
	public Product(String name,Double price){
		this.name = name;
		this.price = price;
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	public int getQty(){
		return this.qty;
	}
	
	public void setQty(int n){
		this.qty = n;
	}
	
}
