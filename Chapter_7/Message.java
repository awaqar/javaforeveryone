package Chapter_7;
/**
 * P7_14*/
public class Message {
	private String sender;
	private String recipient;
	private String msg;
	private String string;
	
	public Message(String sender, String recipient){
		this.sender = sender;
		this.recipient = recipient;
		this.msg = "";
	}
	
	public String getSender(){
		return this.sender;
	}
	
	public String getRecipient(){
		return this.recipient;
	}
	public void append(String text){
		
		 msg = msg + text;
	}
	
	public String toString(){
		string = "From: " + this.sender + "\nTo: " + this.recipient +"\n" + msg;
		return string;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Message msg = new Message("Azum","usman");
		Message msg2 = new Message("anam","azzum");
		msg2.append("hiya!");
		msg.append("hello there you okay?");
		Mailbox mb = new Mailbox();
		mb.addMessage(msg);
		mb.addMessage(msg2);
		mb.showMessages();
		System.out.println(mb.getMessage(0));
	}

}
