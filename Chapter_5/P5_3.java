package Chapter_5;

public class P5_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(countVowels("azzum"));
	}
	
	/**
	 * @param str 
	 * @return number of vowels*/
	public static int countVowels(String str){
		
		 String smg;
		 int vowels = 0;
		for (int i = 0; i < str.length(); i++){
			char ch = str.charAt(i);
			smg = ""+ ch;
			if(smg.matches("[aeiou]")){
				vowels++;
			}
		}
		
		return vowels;
	}

}
