package Chapter_5;

public class P5_19 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = "iphobi";
		System.out.println(isPalindrome(str));
	}
	
	public static boolean isPalindrome(String str){
		
		
		if(str.length()<=1)return true;
			
		else if(str.charAt(0)==str.charAt(str.length()-1)){
			  return isPalindrome(str.substring(1,str.length()-1));
			 
			}
			
		else  return false;
		
	}

}
