package Chapter_5;

import java.util.Scanner;

public class P5_7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		double salary = readDouble("Please enter your salary: ");
		System.out.println(salary);
	}
	
	/**
	 * @param prompt
	 * @return returns the prompt followed by the argument*/
	public static double readDouble(String prompt){
		Scanner in = new Scanner(System.in);
		System.out.print(prompt);
		double num = in.nextDouble();
		return num;
	}

}
