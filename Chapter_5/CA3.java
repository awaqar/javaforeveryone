package Chapter_5;

public class CA3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(reverse("ho"));
		
		System.out.print(readSpace("ho ho"));
	}
	
	
	/**
	 * @param word takes a single string and reverses it
	 * @return returns the reversed string
	 * */
	
	public static String readSpace(String sentence){
		String sent = "";
		String sub = "";
		String rev = "";
		int i = 0;
		char ch= ' ';
		
		while(i < sentence.length()){
			
			 
			if(Character.isWhitespace(sentence.charAt(i))){
				//take the substring from 0 to index of whitespace
				//sub = sentence.substring(0,i);	//ho
				rev = reverse(sub);				//oh
				sent = sent + rev + " ";		// oh_
				//continue;
			}
			
			else{
				sub = sub + sentence.charAt(i); 
			}// 0:h , 1:o  
			
		i++;
		}
		
		return sent;
	}
	
	public static String reverse(String word){
		
		String str ="";
		for(int i = word.length()-1; i >=0; i--){
			str = str + word.charAt(i);
		}
		
		return str;
	}

}
