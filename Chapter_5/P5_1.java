package Chapter_5;

import java.text.DecimalFormat;

public class P5_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DecimalFormat df = new DecimalFormat("#.00");
		int years = 1;
		double interest_rate = 0.05;
		double balance = 100;
		System.out.println("Balance after "+ years + " years = "+ df.format(balance(balance,years,interest_rate)));
	}
	
	public static double balance(double ini_balance, int years, double interest){
		
		int count = 0;
		interest = 1 + interest;
		while (count < years){
			ini_balance = ini_balance * interest;
			count++;
		}
		
		return ini_balance;
	}

}
