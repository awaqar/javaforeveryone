package Chapter_5;

public class P5_20 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean b = find("sip","sip");
		System.out.println(b);
	}
	
	public static boolean find(String str,String match){
		String remainder;
		
		if(str.length()<match.length()) return false;
		
		String msg = str.substring(0,match.length());
		
		if(msg.equals(match)) return true;
		
		else{
			remainder = str.substring(1);
			return find(remainder,match);
		}
	}
}
