package Chapter_5;

public class P5_12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(isLeapYear(1996));
	}
	
	public static boolean isLeapYear(int year){
		boolean LeapYear = false ;
		 if(year % 400 ==0)
			 LeapYear = true;
		
		 else if(year % 100 == 0){
			 return LeapYear;
		 } 
		 
		 else if(year % 4 == 0){
			 LeapYear = true;
		 }
		return LeapYear;
	}

}
