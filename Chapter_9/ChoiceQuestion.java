package Chapter_9;

import java.util.ArrayList;

public class ChoiceQuestion extends Question {
	private ArrayList<String> choices;
	
	public ChoiceQuestion(){
		choices = new ArrayList<String>();
		
	}
	
	public void addChoice(String choice, boolean correct){
		choices.add(choice);
		if(correct){
			String choiceString = "" + choices.size();
			setAnswer(choiceString);
		}
	}
	public void display(){
		super.display();
		int choiceNumber =0;
		for(String x:choices){
			
			choiceNumber++;
			System.out.println(choiceNumber + " : "+ x); 
		}
	}
}
