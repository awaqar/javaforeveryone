package Chapter_9;

public class Question {
	private String text;
	private String answer;
	
	/**
	 * Constructs a question with empty question and answer
	 * */
	public Question(){
		text ="";
		answer = "";
	}
	
	/**
	 * Sets the question text
	 * @param questionText the text of this question
	 * */
	public void setText(String questionText){
		this.text=questionText;
	}
	
	/**
	 * Sets the answer for this question
	 * @param correctResponse the answer
	 * */
	public void setAnswer(String correctResponse){
		this.answer = correctResponse;
	}
	
	/**
	 * Checks a given response for correctness
	 * @param response the reponse to check
	 * @return true if the response was correct, false otherwise
	 * */
	public boolean checkAnswer(String response){
		return response.equalsIgnoreCase(answer);
	}
	
	/**
	 * Displays this question
	 * */
	public void display(){
		System.out.println(this.text);
	}
	
	
}
