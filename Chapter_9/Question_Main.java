package Chapter_9;

import java.util.Scanner;

public class Question_Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Question first = new Question();
		first.setText("Who was the inventor of Java");
		first.setAnswer("James Gosling");
		
		ChoiceQuestion second = new ChoiceQuestion();
		second.setText("In which country was the inventor of Java born?");
		second.addChoice("Australia", false);
		second.addChoice("Canada", true);
		second.addChoice("Denmark",false);
		second.addChoice("USA", false);
		
		presentQuestion(first);
		presentQuestion(second);
	}
	
	public static void presentQuestion(Question q){
		q.display();
		Scanner in = new Scanner(System.in);
		System.out.print("Your answer: ");
		String response = in.next();
		System.out.println(q.checkAnswer(response));
	}

}
